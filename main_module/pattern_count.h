#ifndef PATTERN_COUNT_H
#define PATTERN_COUNT_H

#include "arduino.h"

class PatternCount{
private:
    byte now;
    byte period;
    uint16_t pattern;
public:
    PatternCount():now(0),period(16),pattern(0){};
    PatternCount(byte period):now(0),period(period),pattern(0){};
    bool advance();
    void setPattern(uint16_t pattern);
    uint16_t getPattern();
    void reset();
    void setPeriod(byte period); // period E {1,2,..16}
};

#endif
