#ifndef MPU6050_H
#define MPU6050_H

//#include "arduino.h"
#include <Wire.h>

#define MPU6050_ACCEL_XOUT_H  0x3B
#define MPU6050_ACCEL_YOUT_H  0x3D
#define MPU6050_ACCEL_ZOUT_H  0x3F
#define MPU6050_TEMP_OUT_H    0x3B
#define MPU6050_GYRO_XOUT_H   0x43
#define MPU6050_GYRO_YOUT_H   0x45
#define MPU6050_GYRO_ZOUT_H   0x47

#define MPU6050_WHO_AM_I      0x75
#define MPU6050_PWR_MGMT_1    0x6B
#define MPU6050_ADDRESS       0x68

typedef struct{
    int16_t x;
    int16_t y;
    int16_t z;
}Accelerometer_t;

typedef struct{
    int16_t x;
    int16_t y;
    int16_t z;
}Gyro_t;


class Mpu6050{
public:
    void initialize();
    Accelerometer_t getAccelerometer();
    Gyro_t getGyro();
    int16_t getTemperature();
};

#endif
