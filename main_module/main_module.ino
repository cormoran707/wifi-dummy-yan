#include <Wire.h>
#include <TimerOne.h>
/*
  Wifi-Dummy-yan program(atmega328)

  author : Kenya Ukai
  License : GPL v2 or later
  Develop Environment : ArduinoIDE 1.65
  
 */
#define DEBUG_PC  // シリアルポートをPCに接続するとき

#define LED_PIN 8

#include"dummy_yan.h"

void setup() {
    pinMode(LED_PIN,OUTPUT);
    Serial.begin(115200);
    Dummyyan.initialize();
}


void loop() {
    Dummyyan.sendData();
    Dummyyan.receiveData();
    delay(100);
    digitalWrite(LED_PIN, !digitalRead(LED_PIN));
}
