#include "sound.h"

void Sound::initialize(byte pin){
    this->pin = pin;
    pinMode(pin,OUTPUT);
}

void Sound::setFrequency(uint16_t frequency){
    this->frequency = frequency;
}

uint16_t Sound::getFrequency(){
    return this->frequency;
}

void Sound::on(){
    noTone(this->pin);
    tone(this->pin,this->frequency);
}

void Sound::off(){
    noTone(this->pin);
}
