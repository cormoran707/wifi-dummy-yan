#include"pattern_count.h"


bool PatternCount::advance(){
    if(++now >= period) now = 0;
    return pattern & (1<<now);
}

void PatternCount::setPattern(uint16_t pattern){
    this->pattern = pattern;
}
uint16_t PatternCount::getPattern(){
    return pattern;
}

void PatternCount::reset(){
    now = 0;
}
void PatternCount::setPeriod(byte period){
    if(period > 16)period = 16;
    this->period = period;
    now = 0;
}
