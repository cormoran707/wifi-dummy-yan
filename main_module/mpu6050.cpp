/*
  Wifi-Dummy-yan program(atmega328)

  name mpu6050.ino
  author : Kenya Ukai
  License : GPL v2 or later
  Develop Environment : ArduinoIDE 1.65

  Reference : http://playground.arduino.cc/Main/MPU-6050#short
  
  MPU-6050 とI2C通信して加速度、ジャイロデータを取ってくる部分のプログラム
  
 */

#include"mpu6050.h"

// とりあえず初期値のまま利用
// Gyro at 250 degrees second
// Acceleration at 2g
// Clock source at internal 8MHz
// The device is in sleep mode.

void Mpu6050::initialize(){
    Wire.begin();
    Wire.beginTransmission(MPU6050_ADDRESS);
    Wire.write(MPU6050_PWR_MGMT_1);
    Wire.write(0);
    Wire.endTransmission(true);
}

Accelerometer_t Mpu6050::getAccelerometer(){
    Accelerometer_t acc;
    Wire.beginTransmission(MPU6050_ADDRESS);
    Wire.write(MPU6050_ACCEL_XOUT_H);
    Wire.endTransmission(false);
    Wire.requestFrom(MPU6050_ADDRESS,6,true);
    acc.x = Wire.read()<<8|Wire.read();
    acc.y = Wire.read()<<8|Wire.read();
    acc.z = Wire.read()<<8|Wire.read();
    return acc;
}

Gyro_t Mpu6050::getGyro(){
    Gyro_t gyro;
    Wire.beginTransmission(MPU6050_ADDRESS);
    Wire.write(MPU6050_GYRO_XOUT_H);
    Wire.endTransmission(false);
    Wire.requestFrom(MPU6050_ADDRESS,6,true);
    gyro.x = Wire.read()<<8|Wire.read();
    gyro.y = Wire.read()<<8|Wire.read();
    gyro.z = Wire.read()<<8|Wire.read();
    return gyro;
}

int16_t Mpu6050::getTemperature(){
    Wire.beginTransmission(MPU6050_ADDRESS);
    Wire.write(MPU6050_TEMP_OUT_H);
    Wire.endTransmission(false);
    Wire.requestFrom(MPU6050_ADDRESS,2,true);
    return ( Wire.read()<<8|Wire.read() ) /340.00+36.53;
}



