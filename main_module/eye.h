#ifndef EYE_H
#define EYE_H

/*
  ダミヤン　目

  色 : 黒・赤・緑・黄・青・紫・水・白
  
 */

#include <arduino.h>

enum {
    EYE_COLOR_BLACK,
    EYE_COLOR_RED,
    EYE_COLOR_GREEN,
    EYE_COLOR_BLUE,
    EYE_COLOR_YELLOW,
    EYE_COLOR_MATHENTA,
    EYE_COLOR_CYAN,
    EYE_COLOR_WHITE,
    EYE_COLOR_NUM,
    EYE_COLOR_MANUAL
};

class Eye{
private:
    byte red_pin,green_pin,blue_pin;
    byte rgb[3];
public:
    static const byte COLORS[][3];
    void initialize(byte red_pin,byte green_pin,byte blue_pin);
    void on();
    void off();
    //ret : 引数の色が設定されていた色と違うか？
    bool setColor(byte r,byte g,byte b);
    //ret : 引数の色が設定されていた色と違うか？
    bool setColor(byte color);
    byte getColorR();
    byte getColorG();
    byte getColorB();
};



#endif
