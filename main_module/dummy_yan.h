#ifndef DUMMY_YAN_H
#define DUMMY_YAN_H
#include <TimerOne.h>
#include "eye.h"
#include "sound.h"
#include "mpu6050.h"
#include"pattern_count.h"

/* Pin Settings */
#define LED_R_PIN 4
#define LED_G_PIN 6
#define LED_B_PIN 5
#define SOUND_PIN 10
#define NECK_SENSOR_PIN 0
#define BODY_SENSOR_PIN 1

/* Paramater Settings */
#define NECK_SENSOR_CENTER (1<<5)
#define BODY_SENSOR_CENTER (1<<5)
#define TICK_INTERVAL_MS 500

struct SendData_t{
    char head; // = '#'
    byte rgb[3];
    uint16_t frequency;
    uint16_t pattern;
    int16_t neck_damage;
    int16_t body_damage;
    Accelerometer_t accelero;
    Gyro_t gyro;
    int16_t temperature;
};

struct ReceiveData_t{
    char head; // = '#'
    byte rgb[3];
    uint16_t frequency;
    uint16_t pattern;
    byte command;
};

class DummyyanClass{
private:
public:
    Eye eye;
    Sound sound;
    Mpu6050 mpu;
    PatternCount pattern;
    
    void initialize();
    int getNeckDamage();
    int getBodyDamage();

    void sendData();
    bool receiveData();
    void setData(const ReceiveData_t *rcvdata);
};
extern DummyyanClass Dummyyan;

#endif
