#include"eye.h"

const byte Eye::COLORS[][3] =
{
    {0,0,0},
    {255,0,0},
    {0,255,0},
    {0,0,255},
    {255,255,0},
    {255,0,255},
    {0,255,255},
    {255,255,255}
};


void Eye::initialize(byte red_pin,byte green_pin,byte blue_pin){
    this->red_pin = red_pin;
    this->green_pin = green_pin;
    this->blue_pin = blue_pin;
    pinMode(red_pin,OUTPUT);
    pinMode(green_pin,OUTPUT);
    pinMode(blue_pin,OUTPUT);
    setColor(EYE_COLOR_BLACK);
}

void Eye::on(){
    analogWrite(this->red_pin,this->rgb[0]);
    analogWrite(this->green_pin,this->rgb[1]);
    analogWrite(this->blue_pin,this->rgb[2]);
}

void Eye::off(){
    analogWrite(this->red_pin,0);
    analogWrite(this->green_pin,0);
    analogWrite(this->blue_pin,0);
}

bool Eye::setColor(byte r,byte g,byte b){
    if(this->rgb[0] == r &&
       this->rgb[1] == g &&
       this->rgb[2] == b){
        return false;
    }
    this->rgb[0] = r;
    this->rgb[1] = g;
    this->rgb[2] = b;
    return true;
}

bool Eye::setColor(byte color){
    return setColor(Eye::COLORS[color][0],Eye::COLORS[color][1],Eye::COLORS[color][2]);
}

byte Eye::getColorR(){
    return this->rgb[0];
}

byte Eye::getColorG(){
    return this->rgb[1];
}

byte Eye::getColorB(){
    return this->rgb[2];
}
