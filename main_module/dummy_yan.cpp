#include"dummy_yan.h"

DummyyanClass Dummyyan;

//TICK_INTERVAL_MS ごとに呼ばれる
void tick(){
    if(Dummyyan.pattern.advance()){
        Dummyyan.eye.on();
        Dummyyan.sound.on();
    } else {
        Dummyyan.eye.off();
        Dummyyan.sound.off();
    }
}




void DummyyanClass::initialize(){
    eye.initialize(LED_R_PIN,LED_G_PIN,LED_B_PIN);
    sound.initialize(SOUND_PIN);
    mpu.initialize();
    pattern.setPeriod(12);
    
    pinMode(NECK_SENSOR_PIN,INPUT);
    pinMode(BODY_SENSOR_PIN,INPUT);

    Timer1.initialize(TICK_INTERVAL_MS *1000UL);//us
    Timer1.attachInterrupt(tick);
}

//todo : 何らかの正規化
int DummyyanClass::getNeckDamage(){
    return (int)analogRead(NECK_SENSOR_PIN) - NECK_SENSOR_CENTER;
}

//todo : 何らかの正規化
int DummyyanClass::getBodyDamage(){
    return (int)analogRead(BODY_SENSOR_PIN) - BODY_SENSOR_CENTER;
}

void DummyyanClass::sendData(){
    SendData_t data;
    data.head = '#';
    data.rgb[0]  = eye.getColorR();
    data.rgb[1]  = eye.getColorG();
    data.rgb[2]  = eye.getColorB();
    data.frequency = sound.getFrequency();
    data.pattern = pattern.getPattern();
    data.neck_damage = getNeckDamage();
    data.body_damage = getBodyDamage();
    data.accelero = mpu.getAccelerometer();
    data.gyro = mpu.getGyro();
    data.temperature = mpu.getTemperature();
    Serial.write((char*)&data,sizeof(data));
}

bool DummyyanClass::receiveData(){
    unsigned int rcvbytes = Serial.available();
    if(Serial.available() >= (int)sizeof(ReceiveData_t)){
            while(Serial.peek() != '#' && rcvbytes >0){
                Serial.read();
                --rcvbytes;
            }
            if(rcvbytes >= sizeof(ReceiveData_t)){
                ReceiveData_t rcvdata;
                unsigned int i = 0;
                while(i<sizeof(ReceiveData_t)){
                    *((byte*)((&rcvdata) + i)) = Serial.read();
                    ++i;
                }
                setData(&rcvdata);
                return true;
            }
    }
    return false;
}


void DummyyanClass::setData(const ReceiveData_t *rcvdata){
    eye.setColor(rcvdata->rgb[0],rcvdata->rgb[1],rcvdata->rgb[2]);
    sound.setFrequency(rcvdata->frequency);
    pattern.setPattern(rcvdata->pattern);
}
