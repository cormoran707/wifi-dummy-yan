#ifndef SOUND_H
#define SOUND_H

/*
  ダミヤン 音声

  範囲 : 0.5kHz to 2kHz div 0.25kHz
  
 */

#include <arduino.h>

class Sound{
private:
    uint16_t frequency;
    byte pin;
public:
    void initialize(byte pin);
    void setFrequency(uint16_t frequency);
    uint16_t getFrequency();
    void on();
    void off();
};

#endif
