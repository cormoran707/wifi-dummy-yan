# Wifi Dummy yan #

### What is this repository for? ###

* Wifi通信に対応したダミヤンクローン
* Version 0.1

### 機能 ###

* フルカラーLED x2
* 音
* パターン
* ３軸加速度
* ２軸ジャイロ
* 温度
* 首の変位
* 圧力

### 環境 ###
* ArduinoIDE
* TimerOne Library

### ライセンス ###
* 未定