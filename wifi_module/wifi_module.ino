#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>

#include "id.h"
 
const char *ssid = SSID;
const char *password = PASSWORD;
 
MDNSResponder mdns;
ESP8266WebServer server ( 80 );
const int led = 13;

const char* html = "\
<html> \
	<head> \
		<title>Wifi DummyYan</title> \
		<style> \
		#eye span{display:block; width:49%; height:9em; float: left;margin:0.5%;} \
		#sound span{display:block; background-color:white; width:49%; height:9em; float: left; text-align: center; margin:0.5%;font-size:3em;} \
		</style> \
	</head> \
	<body style='background-color:silver'> \
		<div id='eye'> \
			<span style='width:100%; font-size:4em; font-family:Verdana; text-align: center;height:4em;'>Choose Eye Color</span> \
			<br/> \
			<a href='white'><span style='background-color:white;'/></a> \
			<a href='red'><span style='background-color:red;' /></a> \
			<br/> \
			<a href='green'><span style='background-color:green;' /></a> \
			<a href='blue'><span style='background-color:blue;' /></a> \
			<br/> \
			<a href='yellow'><span style='background-color:yellow;'/></a> \
			<a href='purple'><span style='background-color:fuchsia;' /></a> \
			<br/> \
			<a href='aqua'><span style='background-color:aqua;'/></a> \
			<a href='black'><span style='background-color:black;' /></a> \
		</div> \
		<div id='sound'> \
			<span style='width:100%; font-size:4em; font-family:Verdana; background-color:transparent;height:4em;'>Choose Sound</span> \
			<br/> \
			<a href='0.5khz'><span>0.5kHz</span></a> \
			<a href='0.75khz'><span>0.75kHz</span></a> \
			<br/> \
			<a href='1.0khz'><span>1.0kHz</span></a> \
			<a href='1.25khz'><span>1.25kHz</span></a> \
			<br/> \
			<a href='1.5khz'><span>1.5kHz</span></a> \
			<a href='1.75khz'><span>1.75kHz</span></a> \
			<br/> \
			<a href='1.0khz'><span>2.0kHz</span></a> \
			<a href='0khz'><span>0kHz</span></a> \
		</div> \
	</body> \
</html>";

void handleRoot() {
    server.send ( 200, "text/html", html );
}

void handleNotFound() {
    String message = "ファイルがないよ\n\n";
    message += "URI: ";
    message += server.uri();
    server.send ( 404, "text/plain", message );
}

void setup ( void ) {
    Serial.begin ( 115200 );
    WiFi.begin ( ssid, password );
    Serial.println ( "" );
    // Wait for connection
    while ( WiFi.status() != WL_CONNECTED ) {
        delay ( 500 );
        Serial.print ( "." );
    }
    Serial.println ( "" );
    Serial.print ( "Connected to " );
    Serial.println ( ssid );
    Serial.print ( "IP address: " );
    Serial.println ( WiFi.localIP() );
    if ( mdns.begin ( "esp8266", WiFi.localIP() ) ) {
        Serial.println ( "MDNS responder started" );
    }
    server.on ( "/", []() {handleRoot();} );
    server.on ( "/white", []() {setColor("white"); handleRoot();} );
    server.on ( "/red", []() {setColor("red"); handleRoot();} );
    server.on ( "/green", []() {setColor("green"); handleRoot();} );
    server.on ( "/blue", []() {setColor("blue"); handleRoot();} );
    server.on ( "/yellow", []() {setColor("black"); handleRoot();} );
    server.on ( "/purple", []() {setColor("white"); handleRoot();} );
    server.on ( "/aqua", []() {setColor("red"); handleRoot();} );
    server.on ( "/black", []() {setColor("green"); handleRoot();} );

    server.on ( "/0khz", []() {setColor("0"); handleRoot();} );
    server.on ( "/0.5khz", []() {setColor("500"); handleRoot();} );
    server.on ( "/0.75khz", []() {setColor("750"); handleRoot();} );
    server.on ( "/1.0khz", []() {setColor("1000"); handleRoot();} );
    server.on ( "/1.25khz", []() {setColor("1250"); handleRoot();} );
    server.on ( "/1.5khz", []() {setColor("1500"); handleRoot();} );
    server.on ( "/1.75khz", []() {setColor("1750"); handleRoot();} );
    server.on ( "/2.0khz", []() {setColor("2000"); handleRoot();} );
    server.onNotFound ( handleNotFound );
    server.begin();
    Serial.println ( "HTTP server started" );
}
void loop ( void ) {
    mdns.update();
    server.handleClient();
}

void setColor(const char* str) {
    Serial.print('#');
    Serial.print('E');
    Serial.println (str);
}

void setFrequency(int freq){
    Serial.print('#');
    Serial.print('S');
    Serial.println (freq);
}
